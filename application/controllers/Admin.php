<?php if(!defined('BASEPATH')) exit('No direct scripts Access Allowed');

/**
 * @author : Huda Alfarizi
 * @version : 1.1
 * @since : 15 Mei 2018
 */
class Admin extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
    if($this->session->userdata('status') != "login"){redirect('Login')};
    $this->load->model('Soal_model');
  }

  function index(){
  $data['judul'] = 'CBT';
  $data['main_content'] = 'admin';
  $this->load->view('includes/template',$data);
 }

 function ujian(){
    $id = $this->session->userdata('id');
   $data['judul'] = 'Ujian Online';
   $data['main_content'] = 'ujian';
   $data['soal'] = $this->Soal_model->get_soal($id)->result();
   $this->load->view('includes/template',$data);
 }
}
